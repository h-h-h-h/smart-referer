const WHITELIST_LEGACY_URL  = "https://raw.githubusercontent.com/meh/smart-referer/gh-pages/whitelist.txt";
const WHITELIST_DEFAULT_URL = "dweb:/ipns/QmYoQ4Gn9vAcimaXT5xWYAPrBCu3QZyLmEvhLFu9djNZCy/whitelist.min.txt";


const DWEB_GATEWAYS = Object.freeze([
	// [Protocol Labs – US/CA]
	//  • Terms of Service: https://ipfs.io/legal/ ???
	//     – ✓  That is just a DMCA information page, so no terms on
	//          accessing their site to speak of.
	//  • Privacy Policy: ???
	"https://dweb.link",
	"https://gateway.ipfs.io",
	
	// [Cloudflare – US/CA] as of 03.12.2019:
	//  • Terms of Service: https://www.cloudflare.com/distributed-web-gateway-terms/
	//     – ✓  No terms on accessing their site.
	//  • Privacy Policy: https://www.cloudflare.com/privacypolicy/
	//     – Data Subject type “End Users”
	//     – “[…] information may include but is not limited to IP
	//       addresses, system configuration information, and other
	//       information about traffic to and from Customers’
	//       websites or networks […]“
	"https://cloudflare-ipfs.io",
	
	// [Infura Inc – US/NY]
	//  • Terms of Service: https://infura.io/terms
	//     – ✓  We don't violate any terms.
	//     – “Data Privacy. You consent to the storage of Your
	//        Content in, and transfer of Your Content into, the
	//        regions we operate in, regions in which our servers
	//        are located or regions in which servers relating to
	//        Decentralized Storage Services are located. We will
	//        not access or use Your Content except as necessary to
	//        maintain or provide the Service Offerings, or as
	//        necessary to comply with the law or a binding order
	//        of a governmental body. We will not disclose Your
	//        Content to any government or third party except as
	//        necessary to comply with the law or a binding order of
	//        a governmental body. Unless it would violate the law
	//        or a binding order of a governmental body, we will
	//        give you notice of any legal requirement or order
	//        referred to in this Section 3.2. We will only use your
	//        Account Information in accordance with the Privacy
	//        Policy, and you consent to such usage. The Privacy
	//        Policy does not apply to Your Content.“
	//  • Privacy Policy: https://infura.io/privacy
	//     – Unclear which parts apply to their gateway and how as
	//       section 3.2 of the ToS says it may not apply at all…
	"https://ipfs.infura.io",
	
	// [Pinata Technologies, Inc – US/NB]
	//  • Terms of Service: https://pinata.cloud/terms
	//     – ✓  No terms on accessing their site.
	//  • Privacy Policy: https://pinata.cloud/privacy
	//     – Unclear which parts apply to their gateway and how…
	"https://gateway.pinata.cloud",
	
	// [Peraweb Corp – US/CA] as of 15.11.2019:
	//  • Terms of Use: https://permaweb.io/terms-of-use.html
	//     – 🛇  SHOW STOPPER: “For Use in the United States Only”
	//  • Privacy Policy: https://permaweb.io/privacy-policy.html
	//     – “When you use the Service or otherwise access any of our
	//        Platforms, we may record certain usage information,
	//        such as details about your computer or mobile phone,
	//        the name of your Internet Offerings Provider (ISP),
	//        your IP address, the website from which you visit our
	//        Platforms, the pages on our Platforms that you visit
	//        and in what sequence, and the date and length of your
	//        visit and other statistical information about your
	//        visit to our Platforms. We may also collect aggregated
	//        demographic information regarding users of the Service
	//        and/or our Platforms (e.g., age, gender, and income
	//        information).”
	//     - “When you use the Service or otherwise access any of
	//        our Platforms, we may send “cookies”, “web beacons”,
	//        and similar technologies to your computer or device,
	//        and otherwise employ such technologies, in connection
	//        with operating the Service and our Platforms. These
	//        technologies can identify you as a unique customer
	//        and store your personal preferences as well as product
	//        preferences, tell us which webpages you have visited,
	//        and in what order, and identify your computer or
	//        device. On their own, cookies or web beacons do not
	//        contain or reveal personally identifiable information;
	//        however, if you choose to provide us with personally
	//        identifiable information about you, it can be linked
	//        to the anonymous data stored in the cookies and/or web
	//        beacons.“
	//     – Unclear if they relly meant all of this regarding their
	//       gateway…?
	//"https://gateway.pinata.cloud",
	
	// [RTrade Operations Ltd – CA/BC] as of 22.07.2019:
	//  • Terms of Service: https://gateway.temporal.cloud/ipns/docs.ts.temporal.cloud
	//     – Section 7.2c may be problematic:
	//       “You agree not to: […] (vi) use any robot, spider, or
	//        other automatic device, process, or means to access
	//        the Platform for any purpose, including monitoring or
	//        copying any of the material on the Platform; […]”
	//  • Privacy Policy: https://gateway.temporal.cloud/ipns/docs.pp.temporal.cloud
	//     – “When you use our Services, we may collect information
	//        that is automatically sent tous by your web browser,
	//        such as your numerical IP address. We may also collect
	//        other information, such as the type of browser you use,
	//        which pages you view, and the files you request.”
	//     – Unclear of other parts of their privacy policy
	//       (tracking pixels, Google Analytics, you name it) apply
	//       as well as they always refer to “temporal.cloud” for
	//       those…?
	"https://gateway.temporal.cloud",
	
	// [globalupload.io – ??]
	//  • Privacy Policy: ???
	//     – Hosted through CloudFlare.
	"https://globalupload.io",
]);
